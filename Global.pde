class global{
  //game stuff
  int gameMode = 0;
  int second = 40;
  int bulletTimer = 40;
  
  //scores
  int xtscore = 0;
  int xbscore = 0;
  int yrscore = 0;
  int ylscore = 0;
  int tScore;

  //player and score
  player p = new player(520, 292.5, 3, 100, 100);
  ArrayList<bullet> b = new ArrayList<bullet>();

  //asteroids
  ArrayList<asteroidxt> axt = new ArrayList<asteroidxt>();
  ArrayList<asteroidxb> axb = new ArrayList<asteroidxb>();
  ArrayList<asteroidyr> ayr = new ArrayList<asteroidyr>();
  ArrayList<asteroidyl> ayl = new ArrayList<asteroidyl>();

  boolean fireBullet(){
    if (bulletTimer <= second/5){
      bulletTimer++;
      return false;
    }else{
      return true;
    }
  }

  void start(){
    //update variables
    tScore = xtscore+xbscore+yrscore+ylscore;
    boolean bulletTrue = fireBullet();

    //start drawing
    background(0);
    if (keyPressed == true && key == ' ' && bulletTrue == true){
      bullet bullet = new bullet(p.x, p.y);
      b.add(bullet);
      bulletTimer = 0;
    }
    for (int i = 0; i < b.size(); i++){
      b.get(i).update();
    }
    for (int i = 0; i < b.size(); i++){
      if (b.get(i).kill() == true){
        b.remove(i);
      }
    }
    p.start();

    //the asteroids
    //if (mousePressed == true){
    //  s.xtscore++;
    //  s.xbscore++;
    //  s.yrscore++;
    //  s.ylscore++;
    //}

    while (axt.size() != xtscore+1){
      asteroidxt a = new asteroidxt();
      axt.add(a);
    }

    while (axb.size() != xbscore+1){
      asteroidxb a = new asteroidxb();
      axb.add(a);
    }

    while (ayr.size() != yrscore+1){
      asteroidyr a = new asteroidyr();
      ayr.add(a);
    }

    while (ayl.size() != ylscore+1){
      asteroidyl a = new asteroidyl();
      ayl.add(a);
    }

    for (int i = 0; i < axt.size(); i++){
      if(axt.get(i).reachedGoal() == true){
        asteroidxt a = new asteroidxt();
        axt.set(i, a);
      } else if (axt.get(i).firstTime == true){
        asteroidxt a = new asteroidxt();
        axt.set(i, a);
        axt.get(i).setFirstTimeFalse();
      }
    }

    for (int i = 0; i < axb.size(); i++){
      if(axb.get(i).reachedGoal() == true){
        asteroidxb a = new asteroidxb();
        axb.set(i, a);
      } else if (axb.get(i).firstTime == true){
        asteroidxb a = new asteroidxb();
        axb.set(i, a);
        axb.get(i).setFirstTimeFalse();
      }
    }

    for (int i = 0; i < ayr.size(); i++){
      if(ayr.get(i).reachedGoal() == true){
        asteroidyr a = new asteroidyr();
        ayr.set(i, a);
      } else if (ayr.get(i).firstTime == true){
        asteroidyr a = new asteroidyr();
        ayr.set(i, a);
        ayr.get(i).setFirstTimeFalse();
      }
    }

    for (int i = 0; i < ayl.size(); i++){
      if(ayl.get(i).reachedGoal() == true){
        asteroidyl a = new asteroidyl();
        ayl.set(i, a);
      } else if (ayl.get(i).firstTime == true){
        asteroidyl a = new asteroidyl();
        ayl.set(i, a);
        ayl.get(i).setFirstTimeFalse();
      }
    }

    for (asteroidxt i: axt){
      i.start();
    }

    for (asteroidxb i: axb){
      i.start();
    }

    for (asteroidyr i: ayr){
      i.start();
    }

    for (asteroidyl i: ayl){
      i.start();
    }

    //player-asteroid collision
    for (int i = 0; i < axt.size(); i++){
      if (dist(p.x, p.y, axt.get(i).x, axt.get(i).y) < (p.size/2)+(axt.get(i).size/2)){
        gameMode = 2;
      }
    }

    for (int i = 0; i < axb.size(); i++){
      if (dist(p.x, p.y, axb.get(i).x, axb.get(i).y) < (p.size/2)+(axb.get(i).size/2)){
        gameMode = 2;
      }
    }

    for (int i = 0; i < ayr.size(); i++){
      if (dist(p.x, p.y, ayr.get(i).x, ayr.get(i).y) < (p.size/2)+(ayr.get(i).size/2)){
        gameMode = 2;
      }
    }

    for (int i = 0; i < ayl.size(); i++){
      if (dist(p.x, p.y, ayl.get(i).x, ayl.get(i).y) < (p.size/2)+(ayl.get(i).size/2)){
        gameMode = 2;
      }
    }

    //bullet -asteroid collision
    for (int i = 0; i < axt.size(); i++){
      for (int j = 0; j < b.size(); j++){
        if (dist(b.get(j).x, b.get(j).y, axt.get(i).x, axt.get(i).y) < (b.get(j).size/2)+(axt.get(i).size/2)){
          axt.get(i).manualKill = true;
          b.get(j).manualKill = true;
          xtscore++;
        }
      }
    }
    
    for (int i = 0; i < axb.size(); i++){
      for (int j = 0; j < b.size(); j++){
        if (dist(b.get(j).x, b.get(j).y, axb.get(i).x, axb.get(i).y) < (b.get(j).size/2)+(axb.get(i).size/2)){
          axb.get(i).manualKill = true;
          b.get(j).manualKill = true;
          xbscore++;
        }
      }
    }
    
    for (int i = 0; i < ayr.size(); i++){
      for (int j = 0; j < b.size(); j++){
        if (dist(b.get(j).x, b.get(j).y, ayr.get(i).x, ayr.get(i).y) < (b.get(j).size/2)+(ayr.get(i).size/2)){
          ayr.get(i).manualKill = true;
          b.get(j).manualKill = true;
          yrscore++;
        }
      }
    }
    
    for (int i = 0; i < ayl.size(); i++){
      for (int j = 0; j < b.size(); j++){
        if (dist(b.get(j).x, b.get(j).y, ayl.get(i).x, ayl.get(i).y) < (b.get(j).size/2)+(ayl.get(i).size/2)){
          ayl.get(i).manualKill = true;
          b.get(j).manualKill = true;
          ylscore++;
        }
      }
    }
  }
}
