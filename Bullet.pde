class bullet{
  boolean start = true;
  boolean manualKill = false;
  
  //one time storage
  float xdist;
  float ydist;
  //end
  
  float size = 10;
  float x;
  float y;
  float xIncr;
  float yIncr;
  
  boolean kill(){
    if (manualKill == true || x > 1050+10 || x < -10 || y > 595 || y < - 10){
      return true;
    }else {
      return false;
    }
  }

  bullet(float X, float Y){
    x = X;
    y = Y;
  }

  void update(){
    body();
    movement(50);
  }

  void body(){
    fill(255);
    noStroke();
    ellipse(x, y, size, size);
  }

  void movement(float increment){
    if (start == true){
      xIncr = x-mouseX;
      yIncr = y-mouseY;
      xdist = xIncr;
      ydist = yIncr;
      
      //corrections
      if (xIncr < 0){
        xIncr = -xIncr;
      }
      
      if (yIncr < 0){
        yIncr = -yIncr;
      }
      
      float hypotenuse = sqrt(pow(xIncr, 2)+pow(yIncr, 2));
      float multiplyer = increment/hypotenuse;
      xIncr *= multiplyer;
      yIncr *= multiplyer;
      start = false;
    }
    
    if (xdist < 0 && ydist < 0){
      x += xIncr;
      y += yIncr;
    } else if (xdist > 0 && ydist > 0){
      x -= xIncr;
      y -= yIncr;
    } else if (xdist < 0 && ydist > 0){
      x += xIncr;
      y -= yIncr;
    } else if (xdist > 0 && ydist < 0){
      x -= xIncr;
      y += yIncr;
    }
  }
}
