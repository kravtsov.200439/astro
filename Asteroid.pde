class asteroidxt {
  boolean firstTime = true;
  boolean manualKill = false;
  float x = random(0, 1040);
  float size = random(50, 100);
  float y = 0-size;
  float xspeed = random(-5, 5);
  float yspeed = random(1, 5);

  void setFirstTimeFalse(){
    firstTime = false;
  }

  void makeRatio(){
    x += xspeed;
    y += yspeed;
  }

  boolean reachedGoal(){
    if (manualKill == true || x > 1040+size || x < 0-size || y > 585+size){
      return true;
    } else {
      return false;
    }
  }

  void start(){
    body();
  }

  void body(){
    strokeWeight(3);
    stroke(255);
    noFill();
    ellipse(x, y, size, size);
    makeRatio();
  }
}

class asteroidxb {
  boolean firstTime = true;
  boolean manualKill = false;
  float x = random(0, 1040);
  float size = random(50, 100);
  float y = 585+size;
  float xspeed = random(-5, 5);
  float yspeed = random(1, 5);

  void setFirstTimeFalse(){
    firstTime = false;
  }

  void makeRatio(){
    x -= xspeed;
    y -= yspeed;
  }

  boolean reachedGoal(){
    if (manualKill == true || x > 1040+size || x < 0-size || y < 0-size){
      return true;
    } else {
      return false;
    }
  }

  void start(){
    body();
  }

  void body(){
    strokeWeight(3);
    stroke(255);
    noFill();
    ellipse(x, y, size, size);
    makeRatio();
  }
}

class asteroidyr {
  boolean firstTime = true;
  boolean manualKill = false;
  float y = random(0, 585);
  float size = random(50, 100);
  float x = 0-size;
  float xspeed = random(1, 5);
  float yspeed = random(-3, 3);

  void setFirstTimeFalse(){
    firstTime = false;
  }

  void makeRatio(){
    x += xspeed;
    y += yspeed;
  }

  boolean reachedGoal(){
    if (manualKill == true || y > 585+size || y < 0-size || x > 1040+size){
      return true;
    } else {
      return false;
    }
  }

  void start(){
    body();
  }

  void body(){
    strokeWeight(3);
    stroke(255);
    noFill();
    ellipse(x, y, size, size);
    makeRatio();
  }
}

class asteroidyl {
  boolean firstTime = true;
  boolean manualKill = false;
  float y = random(0, 585);
  float size = random(50, 100);
  float x = 1040+size;
  float xspeed = random(1, 5);
  float yspeed = random(-3, 3);

  void setFirstTimeFalse(){
    firstTime = false;
  }

  void makeRatio(){
    x -= xspeed;
    y -= yspeed;
  }

  boolean reachedGoal(){
    if (manualKill == true || y > 585+size || y < 0-size || x < 0-size){
      return true;
    } else {
      return false;
    }
  }

  void start(){
    body();
  }

  void body(){
    strokeWeight(3);
    stroke(255);
    noFill();
    ellipse(x, y, size, size);
    makeRatio();
  }
}
