global g = new global();

void setup(){
  PImage icon = loadImage("logo.png");
  surface.setIcon(icon);
  surface.setTitle("Astro");
  size(1040, 585);
  frameRate(40);
  smooth();
}

void draw(){
  if(g.gameMode == 0){
    background(0);

    //fill
    noStroke();
    fill(255);
    rect((width/4)+50, (height/8)*3, ((width/4)*2)-100, (height/8)*2, 20);

    //text
    fill(0);
    textAlign(CENTER, CENTER);
    textSize(((width+height)/2)/7);
    text("START", ((width/4)+(width/4+((width/4)*2)))/2, ((((height/8)*3)+(((height/8)*3)+((height/8)*2)))/2)-15);

    //functionalle
    if (mousePressed == true && mouseX >= (width/4)+50 && mouseX <= ((width/4)+50)+(((width/4)*2)-100) && mouseY >= (height/8)*3 && mouseY <= ((height/8)*3)+((height/8)*2)) {
      g.gameMode = 1;
    }
  }else if (g.gameMode == 1){
    g.start();

    //score
    fill(255);
    textAlign(CENTER, TOP);
    textSize(((width+height)/2)/15);
    text(g.tScore, width/2, 5);
  }else if (g.gameMode == 2){
    background(0);

    //UI
    fill(255);
    rect((width/4)*1.01, (height/4)*2*1.01, (width/2)*0.99, ((height/2)*0.97)/2, 20);

    //RESTART
    fill(0);
    textAlign(CENTER, CENTER);
    textSize(((width+height)/2)/7);
    text("RESTART", ((width/4)*1.01+(((width/4)*1.01)+(width/2)*0.99))/2, (((height/4)*2*1.01+((height/4)*2*1.01)+((height/2)*0.97)/2)/2)-15);

    //SCORE
    String scoreText = "SCORE: "+g.tScore;
    fill(255);
    textAlign(CENTER, CENTER);
    textSize(((width+height)/2)/7);
    text(scoreText, ((width/4)*1.01+((width/4)*1.01)+(width/2)*0.99)/2, (((height/4)*1.01+((height/4)*1.01+((height/2)*0.99)/2))/2)-15);

    if (mousePressed == true && mouseX >= (width/4)*1.01 && mouseX <= ((width/4)*1.01)+(width/2)*0.99 && mouseY >= (height/4)*2*1.01 && mouseY <= ((height/4)*2*1.01)+(((height/2)*0.97)/2)) {
      g = new global();
      g.gameMode = 1;
    }
  }
}
