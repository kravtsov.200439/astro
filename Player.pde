class player {
  float x;
  float y;
  float size;
  int thickness;
  int aimRadius;

  player(float X, float Y, int Thickness, int Size, int AimRadius){
    x = X;
    y = Y;
    size = Size;
    thickness = Thickness;
    aimRadius = AimRadius;
  }

  private void updatePos(float increment){
    float x_grad = mouseX/x;
    if (x_grad < 1){
      x_grad -= 1;
      x_grad = -x_grad;
      float x_incr = x_grad/increment;
      x_grad = 1-x_incr;
      x *= x_grad;
    } else if (x > 1){
      float x_incr = (x_grad-1)/increment;
      x_grad = 1+x_incr;
      x *= x_grad;
    }
    float y_grad = mouseY/y;
    if (y_grad < 1){
      y_grad -= 1;
      y_grad = -y_grad;
      float y_incr = y_grad/increment;
      y_grad = 1-y_incr;
      y *= y_grad;
    } else if (y > 1){
      float y_incr = (y_grad-1)/increment;
      y_grad = 1+y_incr;
      y *= y_grad;
    }

    //fixes bug
    if (x < 1){
      x++;
    } else if (y < 1){
      y++;
    }
  }

  void start(){
    aim();
    body();
    updatePos(10);
  }

  private void body(){
    strokeWeight(thickness);
    fill(0);
    stroke(255);
    ellipse(x, y, size, size);
    fill(255);
    ellipse(x, y, size/2, size/2);
    fill(0);
    ellipse(x, y, size/4, size/4);
  }

  private void aim(){
    //aim calculations
    float xdistance = x-mouseX;
    float ydistance = y-mouseY;
    if (xdistance < 0){
      xdistance = -xdistance;
    }
    if (ydistance < 0){
      ydistance = -ydistance;
    }
    float hypotenuse = sqrt(pow(xdistance, 2)+pow(ydistance, 2));
    float multiplyer = aimRadius/hypotenuse;
    float xradius = multiplyer*xdistance;
    float yradius = multiplyer*ydistance;
    //
    strokeWeight(thickness);
    fill(0);
    stroke(255);
    if (x-mouseX < 0 && y-mouseY < 0){
      line(x, y, x+xradius, y+yradius);
    } else if (x-mouseX > 0 && y-mouseY > 0){
      line(x, y, x-xradius, y-yradius);
    } else if (x-mouseX < 0 && y-mouseY > 0){
      line(x, y, x+xradius, y-yradius);
    } else if (x-mouseX > 0 && y-mouseY < 0){
      line(x, y, x-xradius, y+yradius);
    }
  }
}
